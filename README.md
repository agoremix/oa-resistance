R Script and raw data sets associated with the paper:

Agostini, Sylvain, Fanny Houlbreque, Tom Biscéré, Ben P. Harvey, Joshua M. Heitzman, Risa Takimoto, Wataru Yamazaki, Marco Milazzo, and Riccardo Rodolfo Metalpa. ‘Greater Mitochondrial Energy Production Provides Resistance to Ocean Acidification in “Winning” Hermatypic Corals’. Frontiers in Marine Science 7 (2020). https://doi.org/10.3389/fmars.2020.600836.
